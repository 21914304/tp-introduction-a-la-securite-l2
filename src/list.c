#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
<<<<<<< HEAD
    return NULL;
}

int list_is_empty(list_t l){
    return (l==NULL);
}

list_t list_push(list_t l, void* x){
    list_t res = malloc(sizeof(struct cell_t));
    res->val=x;
    res->next=l;

    if(l==NULL)
        res->id=0;
    else
        res->id=1+l->id;
    return res;
}

list_t list_tail(list_t l){
    if(l==NULL)
        return NULL;
    return l->next;
}

void* list_pop(list_t* l){ 
    if(l==NULL)
        return NULL; 
    void* res = (*l)->val;
    list_t tmp = *l;
    (*l) = (*l)->next;
    free(tmp);
    return res;
}

void* list_top(list_t l){
    if(l==NULL)
        return NULL;
    return l->val;
}

void list_destroy(list_t l, void (*free_void)(void*)){
    if(l!=NULL) {
          for(int i=l->id;i>=0;--i){
              free_void(l->val);
              list_t tmp = l;
              l = l->next;
              free(tmp);
          }
    }
=======
  return NULL;
}

int list_is_empty(list_t l){
  return l == NULL;
}

list_t list_push(list_t l, void* x){
  list_t res = malloc(sizeof(struct cell_t));
  res->next = l;
  res->val = x;
  res->id = list_is_empty(l) ? 1 : 1 + l->id;
  return res;
}

list_t list_tail(list_t l){
  return list_is_empty(l) ? NULL : l->next;
}

void* list_pop(list_t* l){
  if(list_is_empty(*l))
    return NULL;
  list_t new_head = (*l)->next;
  void* res = (*l)->val;
  free(*l);
  *l = new_head;
  return res;
}

void* list_top(list_t l){
  return list_is_empty(l) ? NULL : l->val;
}

void list_destroy(list_t l, void (*free)(void*)){
  while(!list_is_empty(l))
    free(list_pop(&l));
>>>>>>> 84c3d114760871e56175b2e90f7c5568b89fb86d
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
<<<<<<< HEAD
    while(l!=NULL) {
        if(eq(x,l->val))
            return l->val;
        l = l->next;
    }
    return NULL;
}

unsigned long int list_len(list_t l){
    if(l==NULL)
        return 0;
    return 1+l->id;
=======
  while(!list_is_empty(l))
    if(eq(x, l->val))
      return l->val;
    else
      l = l->next;
  return NULL;
}

unsigned long int list_len(list_t l){
  return list_is_empty(l) ? 0 : l->id;
>>>>>>> 84c3d114760871e56175b2e90f7c5568b89fb86d
}

