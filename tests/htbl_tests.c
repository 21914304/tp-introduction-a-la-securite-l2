#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "hash_tbl.h"

// char* val
size_t hash(void* val){
  return (*((char*) val)) & ((1<<5) - 1);
}

int eq(void* a, void* b){
  char* aa = (char*) a;
  char* bb = (char*) b;
  for(int i = 0; i <16; i++)
    if(aa[i] != bb[i])
      return 0;
  return 1;
}

void test_empty() {
  hash_tbl htbl = htbl_empty(&hash, &eq, &free);
  assert(htbl->size == 0);
  printf("test_empty OK\n");
}

void test_add() {
  char a[] = "Coucou 1";
  char b[] = "Coucou 2";
  hash_tbl htbl = htbl_empty(&hash, &eq, &free);
  htbl_add(htbl, a);
  assert(htbl->size == 1);
  assert(list_in(htbl->array[hash(a)], a, &eq));
  htbl_add(htbl, a);
  assert(htbl->size == 1);
  assert(list_in(htbl->array[hash(a)], a, &eq));
  htbl_add(htbl, b);
  assert(htbl->size == 2);
  assert(list_in(htbl->array[hash(a)], a, &eq));
  assert(list_in(htbl->array[hash(b)], b, &eq));
  printf("test_add OK\n");
}

void test_in() {
  char a[] = "Coucou 1";
  char b[] = "Coucou 2";
  hash_tbl htbl = htbl_empty(&hash, &eq, &free);
  htbl_add(htbl, a);
  assert(htbl_in(htbl, a));
  assert(!htbl_in(htbl, b));
  htbl_add(htbl, b);
  assert(htbl_in(htbl, a));
  assert(htbl_in(htbl, b));
  printf("test_in OK\n"); 
}

void test_destroy() {
  char a[] = "Coucou 1";
  char b[] = "Coucou 2";
  hash_tbl htbl = htbl_empty(&hash, &eq, &free);
  htbl_add(htbl, a);
  htbl_add(htbl, b);
  htbl_destroy(htbl);
  printf("test_destroy OK\n"); 
}

int main() {
  printf("=== HTBL tests ===\n");
  test_empty();
  test_add();
  test_in();
  test_destroy();

  return 0;
}
  
